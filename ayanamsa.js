function calculateAyanamsa(year) {
    const rateOfPrecession = 360/25772; // Convert arcseconds to degrees
    const yearsSinceJ2000 = year - 2000;
    // Ayanamsa value in degrees
    const ayanamsa = (year-292) * rateOfPrecession;
    return ayanamsa;
}

// Example: Calculate the ayanamsa for 2024
const ayanamsa2024 = calculateAyanamsa(2034);
console.log(`The ayanamsa for 2024 is approximately ${ayanamsa2024.toFixed(4)} degrees.`);

