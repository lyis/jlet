//const pwd = '/home/htaf/lyis/jlet/'
const Astronomy = require('astronomy-engine');
//const nakshatras = require('./nakshatras');

function calculateAyanamsa(year) {
    const rateOfPrecession = 360/25772; // Convert arcseconds to degrees
    const yearsSinceJ2000 = year - 2000;
    // Ayanamsa value in degrees
    const ayanamsa = (year-292) * rateOfPrecession;
    return ayanamsa;
}
const currentYear = new Date().getFullYear();
const ayanamsa = calculateAyanamsa(currentYear);

// Define a "star" at the vernal equinox point
Astronomy.DefineStar('Star1', 0, 0, 1000); // RA=0h, Dec=0°, Distance=1000 light-years

// Function to calculate the Moon's ecliptic longitude relative to the defined "star"

// Assuming 'Star1' has already been defined elsewhere in your code as per the previous example

async function getLunarEclipticLongitude(date) {
    //console.log(`Calculating for date: ${date.toISOString()}`);

    try {
        const sunPos = Astronomy.SunPosition(date);
        const moonEcl = Astronomy.EclipticGeoMoon(date);
	return moonEcl.lon;
    } catch (error) {
        console.error('Error calculating Moon ecliptic longitude relative to Sun:', error);
        throw error; // Re-throw to be caught by caller
    }
}

//// Function to find the current Nakshatra
//async function getCurrentNakshatra(date) {
//  const lunarLongitude = await getLunarEclipticLongitude(date);
//  const siderealLongitude = (lunarLongitude - ayanamsa + 360) % 360;
//  const nakshatraIndex = Math.floor(siderealLongitude / (360 / 27)); // 27 Nakshatras cover the 360 degrees
// // console.log(lunarLongitude, siderealLongitude, nakshatraIndex);
//  return nakshatras[nakshatraIndex];
//}
//async function getCurrentNakshatra(date) {
//  const spicaOppositeLongitude = 180; // Opposite of Spica's approximate ecliptic longitude in degrees
//  const lunarLongitude = await getLunarEclipticLongitude(date);
//  // Adjust lunar longitude using Spica as the reference point
//  const adjustedLongitude = (lunarLongitude + spicaOppositeLongitude - ayanamsa + 360) % 360;
//  const nakshatraIndex = Math.floor(adjustedLongitude / (360 / 27)); // 27 Nakshatras cover the 360 degrees
//
//  // Note: Ensure your nakshatras array is 0-indexed. If it's 1-indexed (starting from 1), adjust accordingly.
//  return nakshatras[nakshatraIndex]; // Assuming nakshatras array is 0-indexed
//}





//async function testMoonEclipticLongitudeRelativeToVernalEquinox() {
//    const testDate = new Date('2024-02-01T00:00:00Z'); // Example date, adjust as needed
//    try {
//        const longitude = await getLunarEclipticLongitude(testDate);
//        console.log(`Test result: The Moon's ecliptic longitude relative to 'Star1' on ${testDate.toISOString()} is ${longitude.toFixed(2)} degrees.`);
//    } catch (error) {
//        console.error('Test failed:', error);
//    }
//}
//
//testMoonEclipticLongitudeRelativeToVernalEquinox();



// Promisify the function to get the Moon's ecliptic longitude
// Adjusted to accept a date parameter for flexibility
//async function getLunarEclipticLongitude(date) { return await getLunarEclipticLongitude(date)};

// Function to determine the Moon's astrological sign based on its ecliptic longitude
function getMoonSign(longitude) {
    const signs = [
        'Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo',
        'Libra', 'Scorpio', 'Sagittarius', 'Capricorn', 'Aquarius', 'Pisces'
    ];
    const siderealLongitude = (longitude - ayanamsa + 360) % 360;
    const index = Math.floor(siderealLongitude / 30);
    return signs[index];
}

    

function getZodiacElement(zodiacSign) {
    const elements = {
        fire: ['Aries', 'Leo', 'Sagittarius'],
        earth: ['Taurus', 'Virgo', 'Capricorn'],
        air: ['Gemini', 'Libra', 'Aquarius'],
        water: ['Cancer', 'Scorpio', 'Pisces']
    };

    for (const [element, signs] of Object.entries(elements)) {
        if (signs.includes(zodiacSign)) {
            return element;
        }
    }

    throw new Error('Invalid Zodiac Sign');
}

// Main function to get and display the current Moon sign
async function currentMoonSign() {
  let date = new Date(); // Define date outside the try block so it's accessible in the catch block
  let longitude; // Declare longitude here so it's accessible throughout the function
  let psas = "";
  try {
    longitude = await getLunarEclipticLongitude(date);
    const sign = getMoonSign(longitude);
    const element = getZodiacElement(sign);
    psas += (`The Moon is currently at ${longitude.toFixed(2)} degrees in ${sign}, an ${element} element.`);
  } catch (error) {
    psas += (`Error determining the Moon sign for ${date.toISOString()} with longitude ${longitude}:`, error);
  }
  return psas;
}


// Helper function to predict the Moon's sign on a future date
async function predictMoonSignOnDate(date) {
    const longitude = await getLunarEclipticLongitude(date);
    const sign = getMoonSign(longitude);
    //console.log(date, longitude, sign);
    return sign;
}


// Function to find the next Moon sign for each element
async function findNextMoonSignsForElements() {
    const currentDate = new Date();
    const result = {
        fire: null,
        air: null,
        water: null,
        earth: null
    };

    // Adjusted to ensure we properly await and check each day for up to 30 days
    for (let minute = 1; minute <= 43200; minute++) {
        const futureDate = new Date(currentDate.getTime() + minute * 60 * 1000);
        try {
            const sign = await predictMoonSignOnDate(futureDate);
	    //console.log(`${futureDate} sign is ${sign}`);
            const element = getZodiacElement(sign);
            
            // Only update if this element's next sign has not been found yet
            if (result[element] === null) {
                result[element] = { sign, date: futureDate };
            }
            
            // Check if all elements have a next sign found
            if (Object.keys(result).every(key => result[key] !== null)) {
                break; // Exit the loop early if each element has a found sign
            }
        } catch (error) {
            console.error(`Error finding sign for future date ${futureDate.toISOString()}:`, error);
            // Consider how to handle this error; for now, we'll just log it
        }
    }

    // Ensure every element has a value before returning
    const allElementsFound = Object.keys(result).every(key => result[key] !== null);
    if (!allElementsFound) {
        console.error("Could not find next signs for all elements within 30 days.");
    }

    return result;
}


// Extend the main function to display the next Moon signs for each element, sorted by date
async function currentMoonSignWithNextElements() {
    let output = "";

    try {
        const currentSignOutput = await currentMoonSign(); // Assuming currentMoonSign now returns a string
        output += currentSignOutput + "\n"; // Add the current Moon sign and element to output
        
        const nextSigns = await findNextMoonSignsForElements();
        output += "Next Moon signs for each element, sorted by date:\n";

        // Convert the nextSigns object into an array of {element, sign, date} objects
        const nextSignsArray = Object.entries(nextSigns).map(([element, info]) => ({
            element,
            sign: info.sign,
            date: info.date
        }));

        // Sort the array by date
        nextSignsArray.sort((a, b) => a.date - b.date);

        // Accumulate the sorted signs into the output string
        nextSignsArray.forEach(({element, sign, date}) => {
            output += `${element}: ${sign} on ${toEastern(date)}\n`;
	    //console.log("found",  `${element}: ${sign} on ${toEastern(date)}\n`)
        });
    } catch (error) {
        output += `Error determining the next Moon signs for elements: ${error.message}\n`;
    }

    return output;
}




function isWithin12Hours(target, base) {
    const hoursDifference = (target.getTime() - base.getTime()) / (1000 * 60 * 60);
    return Math.abs(hoursDifference) <= 12;
}

// Function to find the next lunar apsis after a given event
async function findNextLunarApsis(currentApsis) {
    let nextApsis = Astronomy.SearchLunarApsis(currentApsis.time.date);
    return nextApsis;
}

// Function to find the next lunar node after a given event
async function findNextMoonNode(currentNode) {
    let nextNode = Astronomy.SearchMoonNode(currentNode.time.date);
    return nextNode;
}

// Function to check for any lunar apsis or node today and when the next one will be
async function checkLunarEventsAndNext() {
    const now = new Date();
    let output = `Checking for lunar events around ${toEastern(now)}\n`;

    // Check for the nearest lunar apsis
    try {
        let apsis = Astronomy.SearchLunarApsis(now);
        if (isWithin12Hours(apsis.time.date, now)) {
            const eventType = apsis.kind === 0 ? 'perigee' : 'apogee';
            output += `Lunar ${eventType} at ${apsis.time.date.toISOString()} is within 12 hours.\n`;
        } else {
            output += `No lunar apsis (perigee/apogee) within 12 hours. Finding next...\n`;
            apsis = await findNextLunarApsis(apsis);
            const eventType = apsis.kind === 0 ? 'perigee' : 'apogee';
            output += `Next lunar ${eventType} will be at ${toEastern(apsis.time.date)}.\n`;
        }
    } catch (error) {
        return `Error checking lunar apsis: ${error.message}`;
    }

    // Check for the nearest lunar node
    try {
        let node = Astronomy.SearchMoonNode(now);
        if (isWithin12Hours(node.time.date, now)) {
            const nodeType = node.ascending ? 'ascending' : 'descending';
            output += `Lunar ${nodeType} node at ${toEastern(node.time.date)} is within 12 hours.\n`;
        } else {
            output += `No lunar node (ascending/descending) within 12 hours. Finding next...\n`;
            node = await findNextMoonNode(node);
            const nodeType = node.ascending ? 'ascending' : 'descending';
            output += `Next lunar ${nodeType} node will be at ${toEastern(node.time.date)}.\n`;
        }
    } catch (error) {
        output += `Error checking lunar node: ${error.message}\n`;
    }

    return output;
}

function toEastern(date) {
    // Create an Intl.DateTimeFormat object with desired options, specifying the timezone for Toronto
    const formatter = new Intl.DateTimeFormat('en-CA', { // Using 'en-CA' for Canadian English
        year: 'numeric',
        month: 'short',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        timeZone: 'America/Toronto', // Specify Toronto's timezone
        hour12: false, // Use 24-hour format
        timeZoneName: 'short'
    });

    // Format the date
    return formatter.format(date);
}


function getMoonPhaseAndWaxingWaning() {
    const now = new Date();
    const moonPhase = Astronomy.MoonPhase(now); // Hypothetical function to get the Moon's phase

    // Determine waxing or waning
    let phaseDescription = '';
    let waxingWaning = '';

    if (moonPhase < 90) {
        phaseDescription = 'New Moon to First Quarter';
        waxingWaning = 'Waxing Crescent';
    } else if (moonPhase === 90) {
        phaseDescription = 'First Quarter';
        waxingWaning = 'Waxing';
    } else if (moonPhase < 180) {
        phaseDescription = 'First Quarter to Full Moon';
        waxingWaning = 'Waxing Gibbous';
    } else if (moonPhase === 180) {
        phaseDescription = 'Full Moon';
        waxingWaning = 'Full';
    } else if (moonPhase < 270) {
        phaseDescription = 'Full Moon to Last Quarter';
        waxingWaning = 'Waning Gibbous';
    } else if (moonPhase === 270) {
        phaseDescription = 'Last Quarter';
        waxingWaning = 'Waning';
    } else {
        phaseDescription = 'Last Quarter to New Moon';
        waxingWaning = 'Waning Crescent';
    }

    return `Current Moon Phase: ${Math.round(moonPhase)} degrees ${phaseDescription} (${waxingWaning})`;
}

async function getCurrentNakshatraDetailsAsString() {
  const currentNakshatra = await getCurrentNakshatra(new Date());
  if (!currentNakshatra) {
    return 'Unable to determine the current Nakshatra.';
  }

  let details = `Current Nakshatra: ${currentNakshatra.name} (${currentNakshatra.alias}) `;
  details += `Element: ${currentNakshatra.element} `;
  details += `Ruler: ${currentNakshatra.ruler}\n`;
  details += `Significance: ${currentNakshatra.significance}`;

  return details;
}


Promise.all([/*getCurrentNakshatraDetailsAsString(), */getMoonPhaseAndWaxingWaning(), currentMoonSignWithNextElements(), checkLunarEventsAndNext()])
.then(tx6tslis => {
	tx6tslis.forEach((tx6t) => {
		console.log(tx6t);
	});
}).catch(error => {
    console.error(`An error occurred: ${error}`);
});

