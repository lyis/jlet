const Astronomy = require('astronomy-engine');

function isWithin12Hours(target, base) {
    const hoursDifference = (target.getTime() - base.getTime()) / (1000 * 60 * 60);
    return Math.abs(hoursDifference) <= 12;
}

// Function to find the next lunar apsis after a given event
async function findNextLunarApsis(currentApsis) {
    let nextApsis = Astronomy.SearchLunarApsis(currentApsis.time.date);
    return nextApsis;
}

// Function to find the next lunar node after a given event
async function findNextMoonNode(currentNode) {
    let nextNode = Astronomy.SearchMoonNode(currentNode.time.date);
    return nextNode;
}

// Function to check for any lunar apsis or node today and when the next one will be
async function checkLunarEventsAndNext() {
    const now = new Date();

    console.log(`Checking for lunar events around ${now.toISOString()}`);

    // Check for the nearest lunar apsis
    try {
        let apsis = Astronomy.SearchLunarApsis(now);
        if (isWithin12Hours(apsis.time.date, now)) {
            const eventType = apsis.kind === 0 ? 'perigee' : 'apogee';
            console.log(`Lunar ${eventType} at ${apsis.time.date.toISOString()} is within 12 hours.`);
        } else {
            console.log(`No lunar apsis (perigee/apogee) within 12 hours. Finding next...`);
            apsis = await findNextLunarApsis(apsis);
            const eventType = apsis.kind === 0 ? 'perigee' : 'apogee';
            console.log(`Next lunar ${eventType} will be at ${apsis.time.date.toISOString()}.`);
        }
    } catch (error) {
        console.error(`Error checking lunar apsis: ${error.message}`);
    }

    // Check for the nearest lunar node
    try {
        let node = Astronomy.SearchMoonNode(now);
        if (isWithin12Hours(node.time.date, now)) {
            const nodeType = node.ascending ? 'ascending' : 'descending';
            console.log(`Lunar ${nodeType} node at ${node.time.date.toISOString()} is within 12 hours.`);
        } else {
            console.log(`No lunar node (ascending/descending) within 12 hours. Finding next...`);
            node = await findNextMoonNode(node);
            const nodeType = node.ascending ? 'ascending' : 'descending';
            console.log(`Next lunar ${nodeType} node will be at ${node.time.date.toISOString()}.`);
        }
    } catch (error) {
        console.error(`Error checking lunar node: ${error.message}`);
    }
}

checkLunarEventsAndNext();

