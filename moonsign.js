const Astronomy = require('astronomy-engine');

// Promisify the function to get the Moon's ecliptic longitude
function getMoonEclipticLongitudeAsync() {
  return new Promise((resolve, reject) => {
    try {
      const now = new Date();
      const moonPosition = Astronomy.EclipticLongitude('Moon', now);
      resolve(moonPosition);
    } catch (error) {
      reject(error);
    }
  });
}

// Determine the Moon's astrological sign
function getMoonSign(longitude) {
  const signs = [
    'Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo',
    'Libra', 'Scorpio', 'Sagittarius', 'Capricorn', 'Aquarius', 'Pisces'
  ];
  const index = Math.floor((longitude + 30) % 360 / 30);
  return signs[index];
}

function getZodiacElement(zodiacSign) {
    const elements = {
        fire: ['Aries', 'Leo', 'Sagittarius'],
        earth: ['Taurus', 'Virgo', 'Capricorn'],
        air: ['Gemini', 'Libra', 'Aquarius'],
        water: ['Cancer', 'Scorpio', 'Pisces']
    };

    for (const [element, signs] of Object.entries(elements)) {
        if (signs.includes(zodiacSign)) {
            return element;
        }
    }

    throw new Error('Invalid Zodiac Sign');
}

// Main function to get and display the current Moon sign
async function currentMoonSign() {
  try {
    const longitude = await getMoonEclipticLongitudeAsync();
    const sign = getMoonSign(longitude);
    const element = getZodiacElement(sign);
    console.log(`The Moon is currently in ${sign} an ${element} element.`);
  } catch (error) {
    console.error('Error determining the Moon sign:', error);
  }
}

currentMoonSign();
