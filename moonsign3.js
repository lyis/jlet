const Astronomy = require('astronomy-engine');

Astronomy.DefineStar('Star1', 0, 0, 1000); // RA=0h, Dec=0°, Distance=1000 light-years

async function getMoonEclipticLongitudeRelativeToSun(date) {
    console.log(`Calculating for date: ${date.toISOString()}`);

    try {
        // Get the Sun's position in ecliptic coordinates
        //const starPos = Astronomy.EclipticLongitude("Star1", date);
        const starPos = Astronomy.SunPosition(date);
        console.log(`Sun Ecliptic Longitude: ${starPos.elon} degrees`);

        // Get the Moon's position in ecliptic coordinates
        //const moonEcl = Astronomy.EclipticLongitude('Moon', date);
        const moonEcl = Astronomy.EclipticGeoMoon(date);
	console.log(`Moon Ecliptic Longitude: ${JSON.stringify(moonEcl)} degrees`);

        // Calculate the difference in ecliptic longitude between the Moon and the Sun
        let longitudeDifference = moonEcl.lon;
        console.log(`Initial Longitude Difference: ${longitudeDifference}`);

        // Normalize the difference to be within the range [0, 360)
        longitudeDifference = (longitudeDifference + 360) % 360;
        console.log(`Normalized Longitude Difference: ${longitudeDifference}`);

        return longitudeDifference;
    } catch (error) {
        console.error('Error calculating Moon ecliptic longitude relative to Sun:', error);
        throw error; // Re-throw to be caught by caller
    }
}


async function testMoonEclipticLongitudeRelativeToStar() {
    const testDate = new Date(); // Example date, adjust as needed
    try {
        const longitude = await getMoonEclipticLongitudeRelativeToSun(testDate);
        console.log(`Test result: The Moon's ecliptic longitude relative to 'Vernal Equinox' on ${testDate.toISOString()} is ${longitude.toFixed(2)} degrees.`);
    } catch (error) {
        console.error('Test failed:', error);
    }
}

testMoonEclipticLongitudeRelativeToStar();

