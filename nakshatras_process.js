const fs = require('fs');
const path = require('path');

// Function to parse the Nakshatra descriptions from the text content
function parseNakshatras(text) {
  const nakshatraPattern = /\d+\.\s([A-Za-z\s]+)\(([^)]+)\)\s+Element:\s([A-Za-z]+)\s+Ruler:\s([A-Za-z]+)\s+Significance:\s([\s\S]+?)(?=\d+\.\s[A-Za-z\s]+\(|$)/g;
  const nakshatras = [];
  
  let match;
  while ((match = nakshatraPattern.exec(text)) !== null) {
    nakshatras.push({
      id: nakshatras.length + 1,
      name: match[1].trim(),
      alias: match[2].trim(),
      element: match[3].trim(),
      ruler: match[4].trim(),
      significance: match[5].trim().replace(/\s{2,}/g, ' ')
    });
  }
  
  return nakshatras;
}

// Path to the Nakshatra descriptions text file
const filePath = path.join(__dirname, 'nakshatras.txt');

// Read the text file and process the content
fs.readFile(filePath, 'utf8', (err, data) => {
  if (err) {
    console.error('Error reading the file:', err);
    return;
  }
  
  const nakshatras = parseNakshatras(data);
  console.log(nakshatras);
  
  // Optionally, convert the array to JSON and save it to a new file
  const jsonPath = path.join(__dirname, 'nakshatras.json');
  fs.writeFile(jsonPath, JSON.stringify(nakshatras, null, 2), (err) => {
    if (err) console.error('Error writing JSON file:', err);
    else console.log('Nakshatras JSON saved successfully.');
  });
});

