const fs = require('fs');
const path = require('path');
const { SiderealTime } = require('astronomy-engine');
function calculateLocalSiderealTime(date, longitude) {
    const GAST = SiderealTime(date); // Use your SiderealTime function to get GAST
    const longitudeInHours = longitude / 15; // Convert longitude in degrees to sidereal hours

    // Adjust GAST for local longitude to get Local Sidereal Time (LST)
    let LST = GAST + longitudeInHours;
    
    // Ensure LST is within the range [0, 24)
    LST = (LST + 24) % 24;

    // Convert LST to hours, minutes, and seconds
    const lstHours = Math.floor(LST);
    const lstMinutes = Math.floor((LST - lstHours) * 60);
    const lstSeconds = Math.round(((LST - lstHours) * 60 - lstMinutes) * 60);

    return `${padZero(lstHours)}:${padZero(lstMinutes)}`;//:${padZero(lstSeconds)}`;
}

// Helper function to pad numbers with leading zeros if necessary
function padZero(number) {
    return number.toString().padStart(2, '0');
}

// Example usage for Owen Sound, Ontario
const date = new Date(); // Use current date and time
const longitude = -80.94; // Longitude of Owen Sound, Ontario
const LST = calculateLocalSiderealTime(date, longitude);

//console.log(`Local Sidereal Time in Owen Sound, Ontario: ${LST} (HH:MM:SS).`);
console.log(`LST:${LST}`);
fs.writeFileSync('/tmp/LST.txt', LST, 'utf8');
